var express = require('express');

var app = express();

var todos = require('../models/index.js').todos;

var bodyParser = require('body-parser');

app.use(bodyParser.json());

app.param('todoId', function (req, res, next, todoId) {
	todos.findById(todoId, function (err, result) {
		if (err) {
			req.todoErr = err;
		} else {
			req.todo = result;
		}
		next();
	});
});

app.get('/', function (req, res) {
	res.send('Todo API');
});

app.get('/todos/:todoId', function (req, res) {
	if (req.todoErr) {
		res.error(req.todoErr);
	} else {
		res.send(req.todo);
	}
});

app.get('/todos', function (req, res) {
	var filter;
	if (req.query.filter === 'incomplete') {
		filter.complete = false;
	}
	if (req.query.filter === 'complete') {
		filter.complete = true;
	}
	todos.find(filter, function (err, foundTodos) {
		if (err) {
			res.error(err);
		} else {
			res.json(foundTodos);
		}
	});
});

app.post('/todos', function (req, res) {
	var newTodo;
	if (req.body.id) {
		todos.findById(req.body.id, function (err, foundTodo) {
			if (err) {
				res.error(err);
			} else {
				foundTodo.what = req.body.what;
				foundTodo.complete = req.body.complete;
				foundTodo.save(function (err) {
					if (err) {
						console.error(err);
						res.error(err);
					} else {
						res.json(foundTodo);
					}
				});
			}
		});
	} else {
		newTodo = new todos({
			what: req.body.what,
			complete: req.body.complete
		});
		newTodo.save(function (err) {
			if (err) {
				res.error(err);
			} else {
				res.json(newTodo);
			}
		});
	}
});

app.put('/todos/:todoId', function (req, res) {
	if (req.todoErr) {
		res.error(req.todoErr);
	} else {
		req.todo.what = req.body.what;
		req.todo.complete = req.body.complete;
		req.todo.save(function (err) {
			if (err) {
				res.error(err);
			} else {
				res.json(req.todo);
			}
		});
	}
});

app.delete('/todos', function (req, res) {
	if (req.todoErr) {
		res.error(req.todoErr);
	} else {
		req.todo.what = res.body.what;
		req.todo.complete = res.body.complete;	
		req.todo.remove(function (err) {
			if (err) {
				res.error(err);
			} else {
				req.json(req.todo);
			}
		});
	}
});

module.exports = app;
