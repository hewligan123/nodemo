/* globals __dirname, console */

var express = require('express');

var app = express();

var todos = require('../models/index.js').todos;

var bodyParser = require('body-parser');

var hbs = require('hbs');

var path = require('path');

app.set('view engine', 'html');
app.engine('html', require('hbs').__express);
app.set('views', path.join(__dirname, 'views'));

hbs.registerPartials(path.join(__dirname, 'views', 'partials'));

app.use(bodyParser.urlencoded({extended: true}));

app.get('/list', function (req, res) {
	var filter = {};
	var filterSettings = {
		incomplete: false,
		complete: false
	};
	if (req.query.filter === 'incomplete') {
		filter.complete = false;
		filterSettings.incomplete = true;
	}
	if (req.query.filter === 'complete') {
		filter.complete = true;
		filterSettings.complete = true;
	}
	todos.find(filter, function (err, foundTodos) {
		if (err) {
			console.error(err);
			res.error(err);
		} else {
			res.render('ss-index', {
				todos: foundTodos,
				filterSettings: filterSettings
			});
		}
	});
	
});

app.get('/add', function (req, res) {
	res.render('ss-add');
});

app.get('/update', function (req, res) {
	todos.findById(req.query.id, function (err, result) {
		if (err) {
			console.error(err);
			res.error(err);
		} else {
			res.render('ss-edit', result);
		}
	});
});

app.post('/update', function (req, res) {
	todos.findById(req.body.id, function (err, result) {
		if (err) {
			console.error(err);
			res.error(err);
		} else {
			result.what = req.body['what-todo'];
			if (req.body.complete === 'on') {
				result.complete = true;
			} else {
				result.complete = false;
			}
			result.save(function (err) {
				if (err) {
					console.error(err);
					res.error(err);
				} else {
					res.redirect(app.mountpath + '/list');
				}
			});
		}
	});
});

app.post('/add', function (req, res) {
	var whatTodo = (req.body['what-todo'] || '').toString();
	var newTodo = new todos({
		what: whatTodo
	});
	newTodo.save(function (err, newTodo) {
		if (err) {
			console.error(err);
			res.error(err);
		} else {
			res.redirect(app.mountpath + '/list');
		}
	});
	
});

app.get('/', function (req, res) {
	res.redirect(app.mountpath + '/list');
});

module.exports = app;