/* globals __dirname */

var port = 3000;

var express = require('express');

var app = express();

var serverside = require ('./serverside/index.js');

var api = require('./api/index.js');

var angularServer = require('./angular/index.js');

var reactReduxServer = require('./react-redux/index.js');

var presentation = require('./presentation/index.js');

var path = require('path');

app.set('view engine', 'html');
app.engine('html', require('hbs').__express);
app.set('views', path.join(__dirname, 'views'));

app.use('/serverside', serverside);

app.use('/api', api);

app.use('/angular', angularServer);

app.use('/react-redux', reactReduxServer);

app.use('/presentation', presentation);

app.all('/', function (req, res) {
	res.render('index');
});

app.listen(port);

console.log('Nodemo running on port ' + port);