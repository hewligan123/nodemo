var angular = require('angular');

window.jQuery = require('jquery');
require('bootstrap');
require('bootstrap/dist/css/bootstrap.css');
require('angular-ui-bootstrap');

var todoApp = angular.module('todoApp', ['ui.bootstrap']);

todoApp.constant('TODO_ENDPOINT', '/api/todos');

todoApp.controller('TodoListController', [
	'todoService',
	'$uibModal',
	function (todoService, $uibModal) {
		this.todos = todoService.get();
		this.filterType = 'all';
		this.openAdd = function () {
			$uibModal.open({
				template: require('./views/todoAdd.html'),
				controller: 'TodoAddController',
				controllerAs: 'addCtrl'
			});
		};
		this.applyFilter = function (type) {
			todoService.filter(type);
		};
	}
]);

todoApp.controller('TodoController', [function () {
	var self = this;
	self.editMode = false;
	self.toggleEditMode = function () {
		self.editMode = !self.editMode;
	};
	self.save = function () {
		self.item.update();
		self.toggleEditMode();
	};
}]);

todoApp.controller('TodoAddController', [
	'todoService',
	'$uibModalInstance',
	function (todoService, $uibModalInstance) {
		this.todoText = '';
		this.save = function () {
			todoService.create(this.todoText);
			$uibModalInstance.close();
		};
		this.cancel = function () {
			$uibModalInstance.close();
		};
	}
]);

todoApp.directive('todoApp', [function(){
	return {
		name: 'todoApp',
		scope: {},
		controller: 'TodoListController',
		controllerAs: 'list',
		template: require('./views/todoApp.html')
	};
}]);

todoApp.directive('todoList', [function () {
	// Runs during compile
	return {
		name: 'todoList',
		scope: {
			list: '='
		},
		template: require('./views/todoList.html')
	};
}]);

todoApp.directive('todo', [function () {
	return {
		name: 'todo',
		controller: 'TodoController',
		controllerAs: 'todoCtrl',
		bindToController: true,
		scope: {
			'item': '='
		},
		template: require('./views/todo.html')
	};
}]);

todoApp.factory('todoService', [
	'$http',
	'TODO_ENDPOINT',
	function ($http, TODO_ENDPOINT) {
		var todoList;
		var todos;
		var currentFilter;

		var saveTodo = function (todo) {
			return $http.put(TODO_ENDPOINT + '/' + todo._id, todo);
		};

		var newTodo = function (todo) {
			return $http.post(TODO_ENDPOINT, todo).then(function (result) {
				return result.data;
			});
		};

		var todoTemplate = {
			update: function () {
				saveTodo(this);
				return this;
			},
			markComplete: function () {
				this.complete = true;
				saveTodo(this);
			},
			markIncomplete: function () {
				this.complete = false;
				saveTodo(this);
			}
		};

		var filterTodoList = function (filter) {
			currentFilter = filter;
			todos.length = 0;
			Array.prototype.push.apply(todos, todoList.filter(function (todo) {
				if (! filter || filter === 'all') {
					return true;
				}

				if (filter === 'incomplete' && todo.complete === false) {
					return true;
				}

				if (filter === 'complete' && todo.complete === true) {
					return true;
				}

				return false;
			}));
		};

		var loadTodos = function () {
			return $http.get(TODO_ENDPOINT).then(function (result) {
				return result.data.map(function (todoData) {
					var todo = Object.create(todoTemplate);
					angular.extend(todo, todoData);
					return todo;
				});
			});
		};

		var getTodos = function (filter) {
			if (typeof todoList === 'undefined') {
				todos = [];
				loadTodos().then(function (loadedTodos) {
					todoList = loadedTodos;
					Array.prototype.push.apply(
						todos,
						filterTodoList(filter)
					);
				});
			} else {
				todos = filterTodoList(filter);
			}

			return todos;
		};

		var createTodo = function (todoText) {
			var todo = Object.create(todoTemplate);
			todo.what = todoText;
			todo.complete = false;
			todoList.push(todo);
			newTodo(todo).then(function (serverTodo) {
				Object.assign(todo, serverTodo);
			});
			filterTodoList(currentFilter);
			return todo;
		};

		return {
			get: getTodos,
			create: createTodo,
			filter: filterTodoList
		};
	}
]);
