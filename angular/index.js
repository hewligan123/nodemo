/* globals __dirname */

var express = require('express');

var app = express();

var path = require('path');

app.set('view engine', 'html');
app.engine('html', require('hbs').__express);
app.set('views', path.join(__dirname, 'views'));

app.use('/static', express.static(path.join(__dirname, 'static')));

app.get('/', function (req, res) {
	res.render('angular-index');
});

module.exports = app;