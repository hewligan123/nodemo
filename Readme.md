#Nodemo#

Todo app implemented in 3 1/2 ways.

1. Pure serverside using handlebars
1. Angular JS
1. ReactJS and Redux

And the half is an API backend for the angular and react apps.

Uses MongoDB for backend storage.

Run using:
```
#!basg
npm start
```


Rebuild webpack bundles for Angular & React using:
```
#!basg
npm run build
```