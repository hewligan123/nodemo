/* globals __dirname */

var express = require('express');

var app = express();

var path = require('path');

app.use('/', express.static(path.join(__dirname, 'static')));

module.exports = app;