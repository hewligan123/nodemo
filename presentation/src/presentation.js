var Reveal = require('reveal');

require('reveal/index.css');
require('reveal/theme/default.css');

Reveal.initialize({
	controls: true,
	progress: true,
	history: true,
	center: true,
	// default/cube/page/concave/zoom/linear/fade/none 
	transition: 'concave'
});