/* globals __dirname */

var express = require('express');
var todos = require('../models/index.js').todos;

var app = express();

var path = require('path');

require('babel-register');
require('babel-polyfill');

var renderPage = require('./server/renderPage.js').renderPage;

app.use('/static', express.static(path.join(__dirname, 'static')));

app.get('/', function (req, res) {
	todos.find(function (err, foundTodos) {
		res.send(renderPage(foundTodos));
	});
});

module.exports = app;

