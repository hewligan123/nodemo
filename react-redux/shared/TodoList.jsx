import React from 'react';
import Todo from './Todo.jsx';

export default class TodoList extends React.Component {
	render () {
		return(
			<ul className="list-group">
				{this.props.todos.map(todo =>
					<Todo 
						todo={todo} 
						key={todo.get('_id')}
						markComplete={() => this.props.markComplete(todo)}
						markIncomplete={() => this.props.markIncomplete(todo)}
						updateTodo={(what) => this.props.updateTodo(todo, what)}
					></Todo>
				)}
			</ul>
		);
	}
}
