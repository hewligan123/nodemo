import React from 'react';

export default class Todo extends React.Component {
	state = {
		edit: false,
		what: this.props.todo.get('what')
	}

	markComplete = () => {
		this.props.markComplete()
	}

	markIncomplete = () => {
		this.props.markIncomplete()
	}

	setWhat = (event) => {
		this.setState({what: event.target.value});
	}

	toggleEdit = (event) => {
		this.setState({edit: !this.state.edit});
	}

	update = (event) => {
		this.props.updateTodo(this.state.what);
		this.toggleEdit();
	}

	render () {
		return <li className="list-group-item">
			<div className="todo-item row clearfix">
				<div className="todo-item-description col-xs-8">
					{this.state.edit ?
						<input type="text" defaultValue={this.state.what} onChange={this.setWhat}></input>
					:
						<span>{this.props.todo.get('what')}</span>
					}
				</div>
				<div className="col-xs-2">
					{this.state.edit ?
						<button className="btn btn-primary" type="button" onClick={this.update}>Save</button>
					:
						<button className="btn btn-primary" type="button" onClick={this.toggleEdit}>Edit</button>
					}
				</div>
				<div className="todo-item-complete col-xs-2">
					{this.props.todo.get('complete') ?
						<button className="btn btn-success" onClick={this.markIncomplete}>
							<i className="glyphicon glyphicon-ok "></i>
						</button>
					:
						<button className="btn btn-danger" onClick={this.markComplete}>
							<i className="glyphicon glyphicon-minus-sign"></i>
						</button>
					}
				</div>
			</div>
		</li>
	}
}