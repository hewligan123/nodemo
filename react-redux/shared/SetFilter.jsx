import React from 'react';
import { connect } from 'react-redux';
import {actions} from './todoStore.js';

const mapStateToProps = (state) => {
	return {
		filter: state.get('filter')
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setFilter: (filter) => {
			dispatch(actions.setFilter(filter));
		}
	}
};

class SetFilter extends React.Component {
	updateFilter = (event) => {
		this.props.setFilter(event.target.value);
	}

	render () {
		return (
			<div className="well">
				<div className="form-group">
					<label htmlFor="filter">Filter todos</label>
					<select value={this.props.filter} onChange={this.updateFilter} className="form-control" name="filter" id="filter">
						<option value="all" >All</option>
						<option value="incomplete">To Do</option>
						<option value="complete">Done</option>
					</select>
				</div>
			</div>
		);
	}
};



export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SetFilter)
