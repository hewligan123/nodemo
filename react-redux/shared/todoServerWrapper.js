import fetch from 'isomorphic-fetch';

const ENDPOINT = '/api/todos';


const headers = new Headers({
	'Content-Type': 'application/json'
});

const makeTodo = function (what) {
	const todo = {
		what: what,
		complete: false
	};
	return fetch(ENDPOINT, {
		method: 'POST',
		headers: headers,
		body: JSON.stringify(todo)
	}).then(function (response) {
		return response.json();
	});
};

const updateTodo = function (todo) {
	return fetch(ENDPOINT + '/' + todo._id, {
		method: 'PUT',
		headers: headers,
		body: JSON.stringify(todo)
	}).then(function (response) {
		return response.json();
	});
};

const todoServerWrapper = {
	makeTodo,
	updateTodo
};

export default todoServerWrapper;