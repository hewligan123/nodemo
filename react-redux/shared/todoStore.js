import { List, Map } from 'immutable';
import todoServerWrapper from './toDoServerWrapper.js';

const actionTypes = Map({
	addTodo: 'ADD_TODO',
	updateTodo: 'UPDATE_TODO',
	markComplete: 'MARK_COMPLETE',
	markIncomplete: 'MARK_INCOMPLETE',
	setFilter: 'SET_FILTER'
});

const addTodo = what => {
	return (dispatch) => {
		todoServerWrapper.makeTodo(what).then((newTodo) => {
			dispatch({
				type: actionTypes.get('addTodo'),
				payload: Map(newTodo)
			});
		});
	};
	
};

const updateTodo = (todo, what) => {
	return (dispatch) => {
		let updatedTodo = todo.toJS();
		updatedTodo.what = what;
		todoServerWrapper.updateTodo(updatedTodo).then((newTodo) => {
			dispatch({
				type: actionTypes.get('updateTodo'),
				payload: {
					_id: newTodo._id,
					what: newTodo.what
				}
			});
		});
	};
};

const markComplete = todo => {
	return (dispatch) => {
		let updatedTodo = todo.toJS();
		updatedTodo.complete = true;
		todoServerWrapper.updateTodo(updatedTodo).then((newTodo) => {
			dispatch({
				type: actionTypes.get('markComplete'),
				payload: {
					_id: newTodo._id
				}
			});
		});
	};
};

const markIncomplete = todo => {
	return (dispatch) => {
		let updatedTodo = todo.toJS();
		updatedTodo.complete = false;
		todoServerWrapper.updateTodo(updatedTodo).then((newTodo) => {
			dispatch({
				type: actionTypes.get('markIncomplete'),
				payload: {
					_id: newTodo._id
				}
			});
		});
	};
};

const setFilter = filter => {
	return {
		type: actionTypes.get('setFilter'),
		payload: {
			filter
		}
	};
};



export const actions = {
	addTodo,
	updateTodo,
	markComplete,
	markIncomplete,
	setFilter
};


const init = Map({
	filter: 'all',
	todos: List()
});

export const todoReducer = function (store = init, action) {
	switch (action.type) {
	case actionTypes.get('addTodo'):
		{
			const todos = store.get('todos').push(Map(action.payload));
			return store.set('todos', todos);
		}
	case actionTypes.get('updateTodo'):
		{
			const todos = store.get('todos').map(todo =>  {
				if (todo.get('_id') === action.payload._id) {
					return todo.set('what', action.payload.what);
				}
				return todo;
			});
			return store.set('todos', todos);
		}
	case actionTypes.get('markComplete'):
		{
			const todos = store.get('todos').map(todo => {
				if (todo.get('_id') === action.payload._id) {
					return todo.set('complete', true);
				}
				return todo;
			});
			return store.set('todos', todos);
		}
	case actionTypes.get('markIncomplete'):
		{
			const todos = store.get('todos').map(todo => {
				if (todo.get('_id') === action.payload._id) {
					return todo.set('complete', false);
				}
				return todo;
			});
			return store.set('todos', todos);
		}
	case actionTypes.get('setFilter'):
		return store.set('filter', action.payload.filter);
	default:
		return store;
	}
};