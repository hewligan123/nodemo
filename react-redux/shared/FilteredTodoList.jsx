import React from 'react';
import { connect } from 'react-redux';

import {actions} from './todoStore.js';
import TodoList from './TodoList.jsx';

const applyFilter = (todos, filter) => {
	if (!filter || filter === 'all') {
		return todos;
	}
	return todos.filter(todo => {
		if (filter === 'complete' &&  todo.get('complete')) {
			return true;
		}
		if (filter === 'incomplete' && !todo.get('complete')) {
			return true;
		}
		return false;
	});
};

const mapStateToProps = (state) => {
	return {
		todos: applyFilter(state.get('todos'), state.get('filter'))
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		markComplete: (id) => {
			dispatch(actions.markComplete(id));
		},
		markIncomplete: (id) => {
			dispatch(actions.markIncomplete(id));
		},
		updateTodo: (id, what) => {
			dispatch(actions.updateTodo(id, what));
		}
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TodoList)