import React from 'react';
import { connect } from 'react-redux';
import { Modal } from 'react-bootstrap';
import { Map } from 'immutable';

import {actions} from './todoStore.js';

class AddTodo extends React.Component {
	state = {
		modalIsOpen: false,
		what: ''
	}

	openModal = () => {
		this.setState({modalIsOpen: true});
	}

	closeModal = () => {
		this.setState({modalIsOpen: false});
	}

	setWhat = (event) => {
		this.setState({what: event.target.value});
	}

	save = () => {
		this.props.addTodo(this.state.what);
		this.closeModal();
	}

	render () {
		return (
			<div>
				<button type="button" className="btn btn-primary" onClick={this.openModal}>
					<i className="glyphicon glyphicon-plus"></i> Add Todo
				</button>
				<Modal show={this.state.modalIsOpen}>
					<Modal.Header>
						<Modal.Title>Add new todo</Modal.Title>	
					</Modal.Header>
					<Modal.Body>
						<input type="text" value={this.state.what} onChange={this.setWhat}></input>
					</Modal.Body>
					<Modal.Footer>
						<button className="btn btn-primary" type="button" onClick={this.save}>Save</button>
					    <button className="btn btn-warning" type="button" onClick={this.closeModal}>Cancel</button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		addTodo: (what) => dispatch(actions.addTodo(what))
	};
};

export default connect(null, mapDispatchToProps)(AddTodo);