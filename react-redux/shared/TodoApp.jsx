import React from 'react';

import FilteredTodoList from './FilteredTodoList.jsx';
import SetFilter from './SetFilter.jsx';
import AddTodo from './AddTodo.jsx';

export default class TodoApp extends React.Component {
	render () {
		return(
			<div>
				<h2>My ToDos</h2>
				<SetFilter></SetFilter>
				<FilteredTodoList></FilteredTodoList>
				<AddTodo></AddTodo>
			</div>
		);
	}
};