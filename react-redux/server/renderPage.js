import {createStore} from 'redux';
import {fromJS} from 'immutable';
import page from './page';
import {todoReducer} from '../shared/todoStore';

export function renderPage(todos) {
	const store = createStore(todoReducer, fromJS({
		filter: 'all',
		todos
	}));
	return page(store);
}