import React from 'react';
import {renderToString, renderToStaticMarkup} from 'react-dom/server';
import TodoApp from '../shared/ToDoApp';
import {Provider} from 'react-redux';

class Page extends React.Component {
	setInitialState () {
		return {
			__html: 'window.__initialState = '
				+ JSON.stringify(this.props.store.getState())
				+';'
		};
	}
	renderInitialApp () {
		return {
			__html: renderToString(
				<Provider store={this.props.store}>
					<TodoApp></TodoApp>
				</Provider>)
		};
	}

	render () {
		return (
			<html lang="en">
				<head>
					<meta charSet="UTF-8"></meta>
					<title>TODO - React-Redux</title>
					<link rel="stylesheet" href="react-redux/static/todo-styles.css" />
				</head>
				<body>
					<div>
						<div className="jumbotron">
							<div className="container">
								<h1 className="page-header">TODOs - React-Redux</h1>		
							</div>
						</div>
						<div id="todo-app-root" className="container" dangerouslySetInnerHTML={this.renderInitialApp()}>
							
						</div>
					</div>
					<script dangerouslySetInnerHTML={this.setInitialState()}></script>
					<script src="react-redux/static/todo-bundle.js" />
				</body>
			</html>
		);
	}
};

export default function (store) {
	return '<!DOCTYPE html>' + renderToStaticMarkup(<Page store={store} />);
}