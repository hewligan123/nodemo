import 'babel-polyfill';

//Need to use require to import bootstrap. Can I fix this?
require('bootstrap/dist/css/bootstrap.css');

import { todoReducer }   from '../shared/todoStore.js';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { fromJS } from 'immutable';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import TodoApp from '../shared/TodoApp.jsx';

const store = createStore(todoReducer, fromJS(window.__initialState), applyMiddleware(thunk));
const appRoot = document.getElementById('todo-app-root');

render(
	<Provider store={store}>
		<TodoApp></TodoApp>
	</Provider>,
	appRoot
);