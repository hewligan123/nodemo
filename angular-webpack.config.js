var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: './angular/src/todoApp.js',
	output: {
		path: './angular/static',
		filename: 'todo-bundle.js'
	},
	devtool: 'source-map',
	module: {
		loaders: [
			{
				test: /\.html$/,
		        loader: 'raw'
			},
			{ 
				test: /\.css$/, 
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
			},
			{test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
	      	{test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
	      	{test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
	      	{test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
		]
	},
	plugins: [
        new ExtractTextPlugin('angular-styles.css'),
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.optimize.DedupePlugin()
    ]
};
