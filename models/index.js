var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/nodemo');

var todoSchema = new mongoose.Schema({
	what: String,
	complete: {type: Boolean, default: false}
});

var todoModel = mongoose.model('todo', todoSchema);

module.exports = {
	todos: todoModel
};