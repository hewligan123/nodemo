var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: './presentation/src/presentation.js',
	output: {
		path: './presentation/static',
		filename: 'presentation.js'
	},
	devtool: 'source-map',
	module: {
		loaders: [
			{
				test: /\.html$/,
		        loader: 'raw'
			},
			{ 
				test: /\.css$/, 
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
			},
			{test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
	      	{test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
	      	{test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
	      	{test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
		]
	},
	plugins: [
        new ExtractTextPlugin('presentation-styles.css'),
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.optimize.DedupePlugin()
    ]
};
